# Django-totorial-blog

This repo is Django Tutorial from :<br>
<https://carolhsu.gitbooks.io/django-girls-tutorial-traditional-chiness/content/deploy/README.html>

- For safety and production reason, you need to create database on your local machine after clone this repo
	- Step1. create new database :<br>
		Execute `run_migrate_SyncDatabase.bat` directly, Or
		```
		# Change to project folder
		cd <Project-Folder>
		
		# create database according model.py
		python manage.py migrate
		```
	- Step2. create new login accont :<br>
		`python manage.py createsuperuser`	
		
- Use `run_Server.bat` to activate django server, and navigate to `localhost:8000` in browser.

- Navigate to `localhost:8000/admin` for backend management.

